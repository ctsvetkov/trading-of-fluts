package ttsvetkov.tradingfluts;

public interface SchuurScanner {
    /**
     * Scan int
     * @return
     */
    int nextInt();

    /**
     * Scan whole line
     * @return trimmed line
     */
    String nextLine();

    /**
     * Scan Schuur
     * @param z number of piles to scan
     * @return
     */
    Schuur nextSchuur(int z);

    /**
     * Scan Pile
     * @return
     */
    Pile nextPile();

    /**
     * Free resources
     */
    void close();
}