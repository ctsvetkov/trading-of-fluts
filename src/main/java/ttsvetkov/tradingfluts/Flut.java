package ttsvetkov.tradingfluts;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public class Flut implements Comparable<Flut>{
    private Integer price;

    public Flut() {
        this.price = 0;
    }

    public Flut(Integer price) {
        this.price = price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPrice() {
        return this.price.hashCode();
    }

    public int hashCode() {
        return price.hashCode();
    }

    public boolean equals(Object o) {
        if (o == this)
            return true;
        
        if (!(o instanceof Flut))
            return false;
        
        Flut other = (Flut) o;
        
        return (this.price == null && other.price == null)
            || (this.price != null && this.price.equals(other.price));
    }

    public String toString() {
        return "Flut {" + price + "}";
    }

    public int compareTo(Flut o) {
        return this.price.compareTo(o.price);
    }
}
