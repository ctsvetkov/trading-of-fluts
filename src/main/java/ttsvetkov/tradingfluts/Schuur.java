package ttsvetkov.tradingfluts;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public class Schuur {
    private List<Pile> piles;

    public Schuur(Integer z) {
        this.piles = new ArrayList<Pile>(z);
    }

    /**
     * Copy constructor
     * @param s
     */
    public Schuur(Schuur s) {
        this.piles = new ArrayList<Pile>(s.countPiles());

        for(Pile p: s.getPiles()) {
            addPile(new Pile(p));
        }
    }

    /**
     * Add new pile to the piles list
     * @param p
     * @return
     */
    public boolean addPile(Pile p) {
        return this.piles.add(p);
    }

    /**
     * @return piles in this Schuur
     */
    public List<Pile> getPiles() {
        return this.piles;
    }

    /**
     * Get the number of piles in this Schuur
     * @return
     */
    public int countPiles() {
        return piles.size();
    }

    public int hashCode() {
        return piles.hashCode();
    }

    public boolean equals(Object o) {
        if (o == this)
            return true;
        
        if (!(o instanceof Schuur))
            return false;
        
        Schuur other = (Schuur) o;
        
        return (this.piles == null && other.piles == null)
            || (this.piles != null && this.piles.equals(other.piles));
    }

    public String toString() {
        return "Schuur {" + piles.toString() + "}";
    }
}
