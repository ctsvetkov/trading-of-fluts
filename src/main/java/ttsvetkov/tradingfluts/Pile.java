package ttsvetkov.tradingfluts;

import java.util.LinkedList;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@SuppressWarnings("serial")
public class Pile extends LinkedList<Flut> {
    /**
     * No arg constructor
     */
    public Pile () {
    }

    /**
     * Copy constructor
     * @param p
     */
    public Pile(Pile p) {
        super(p);
    }
}
