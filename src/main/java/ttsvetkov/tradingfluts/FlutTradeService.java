package ttsvetkov.tradingfluts;

import java.util.List;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public interface FlutTradeService {
    /**
     * Process the Schuur and generate internal profitList
     */
    void process(final Schuur s);

    /**
     * Get generated internal ProfitList
     * @return
     */
    List<List<Integer>> getProfitList();

    /**
     * Get the max profit you can achieve
     * @return
     */
    Integer getMaxProfit();

    /**
     * Get List of the 10 smallest number of fluts the merchant has to buy to obtain the max profit.
     * @return
     */
    List<Integer> getBuyList();
}