package ttsvetkov.tradingfluts;

import java.io.InputStream;
import java.util.Scanner;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public class SchuurScannerImpl implements SchuurScanner {
    private Scanner sc;

    SchuurScannerImpl(InputStream in) {
        this.sc = new Scanner(in);
    }

    SchuurScannerImpl(String in) {
        this.sc = new Scanner(in);
    }

    @Override
    public void close() {
        sc.close();
    }

    @Override
    public int nextInt() {
        return sc.nextInt();
    }

    public String nextLine() {
        return sc.nextLine().trim();
    }

    @Override
    public Schuur nextSchuur(int z) {
        if (z < 1) {
            throw new IllegalArgumentException("Invalid number of Piles");
        }

        Schuur schuur = new Schuur(z);

        for (int i = 0; i < z; i++) {
            schuur.addPile(nextPile());
        }

        return schuur;
    }

    @Override
    public Pile nextPile() {
        Pile pile = new Pile();
        String[] input = nextLine().trim().split("\\s+");

        /**
         * number of tuples to scan;
         */
        int n = Integer.valueOf(input[0]);
        if (n < 1 || n > (input.length - 1)) {
            throw new RuntimeException("Invalid pile input");
        }

        for(int i = 0; i < n; i++) {
            pile.add(new Flut(Integer.parseInt(input[i + 1])));
        }

        return pile;
    }
}