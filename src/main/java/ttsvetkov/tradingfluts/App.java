package ttsvetkov.tradingfluts;

import java.util.ArrayList;
import java.util.List;

/**
 * Trading of fluts
 */
public class App 
{
    public static void main( String[] args )
    {
        int z = 1;
        List<Schuur> schuurList = new ArrayList<Schuur>();
        SchuurScanner sc = new SchuurScannerImpl(System.in);
 
        while (z > 0) {
            z = Integer.parseInt(sc.nextLine());
            if (z > 0) {
                schuurList.add(sc.nextSchuur(z));
            }
        }

        sc.close();

        System.out.println();

        FlutTradeService tradeService = new FlutTradeServiceImpl();

        for (int i = 0; i < schuurList.size(); i++) {
            System.out.println("schuurs " + (i + 1));

            tradeService.process(schuurList.get(i));

            System.out.println(String.format("Maximum profit is %d.", tradeService.getMaxProfit()));
            
            System.out.print("Number of fluts to buy:");
            tradeService.getBuyList()
                .forEach(item -> System.out.print(" " + item));
            System.out.println();
        }
    }
}
