package ttsvetkov.tradingfluts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public class FlutTradeServiceImpl implements FlutTradeService {
    /**
     * The Flut price we gat in Holland.
     * @TODO: move this into configuration
     */
    public static final int SELL_PRICE = 10;

    /**
     * Max flut to buy List size
     */
    public static final int FLUTS_TO_BUY_SIZE = 10;

    private List<List<Integer>> profitList;

    private Integer maxProfit;

    public FlutTradeServiceImpl() {
        profitList = new ArrayList<List<Integer>>();
    }

    @Override
    public void process(final Schuur schuur) {
        profitList.clear();

        Integer profit = 0;
        List<Pile> pList = schuur.getPiles();
        int countPiles = schuur.countPiles();
        int pileMaxProfit = 0;

        maxProfit = 0;
        for (int i = 0; i < countPiles; i++) {
            Pile pile = pList.get(i);

            List<Integer> pileProfitList = new ArrayList<Integer>(pile.size() + 1);
            pileProfitList.add(0);

            pileMaxProfit = 0;
            profit = 0;

            for (int j = 0; j < pile.size(); j++) {
                profit += SELL_PRICE - pile.get(j).getPrice();

                pileProfitList.add(profit);

                if (profit > pileMaxProfit) {
                    pileMaxProfit = profit;
                }
            }

            if (pileMaxProfit > 0) {
                maxProfit += pileMaxProfit;
                pileProfitList.set(0, pileMaxProfit);
                profitList.add(pileProfitList);
            }
        }
    }

    @Override
    public List<List<Integer>> getProfitList() {
        return profitList;
    }

    @Override
    public Integer getMaxProfit() {
        if (profitList.isEmpty() || null == maxProfit) {
            throw new UnsupportedOperationException("Please call process method first!");
        }

        return maxProfit;
    }

    @Override
    public List<Integer> getBuyList() {
        if (profitList.isEmpty() || null == maxProfit) {
            throw new UnsupportedOperationException("Please call process method first!");
        }

        if (0 == maxProfit) {
            List<Integer> list = new ArrayList<Integer>(1);
            list.add(0);
            return list;
        }

        Set<Integer> flutsToBuySet = new TreeSet<Integer>();
        List<List<Integer>> buyList = findBestProfitListToBuy();

        int n = buyList.size();
        int[] itr = new int[n];

        while (itr[n -1] < buyList.get(n - 1).size()) {
            int sum = 0;
            for (int i = 0; i < n; i++) {
                sum += buyList.get(i).get(itr[i]);
            }
            
            flutsToBuySet.add(sum);

            for (int i = 0; i < n; i++) {
                itr[i]++;
                if (itr[i] < buyList.get(i).size()) {
                    break;
                } else if (i == (n -1)) {
                    break;
                } else {
                    itr[i] = 0;
                }
            }
        }

        return flutsToBuySet.stream().limit(FLUTS_TO_BUY_SIZE).collect(Collectors.toList());
    }

    private  List<List<Integer>> findBestProfitListToBuy() {
        List<List<Integer>> bestList = new ArrayList<List<Integer>>(profitList.size());

        for (int i = 0; i < profitList.size(); i++) {
            List<Integer> pList = profitList.get(i);
            int profit = pList.get(0);

            List<Integer> addList = new LinkedList<Integer>();
            for (int j = 1; j < pList.size(); j++) {
                if (pList.get(j) == profit) {
                    addList.add(j);
                }
            }

            bestList.add(addList);
        }

        return bestList;
    }
}