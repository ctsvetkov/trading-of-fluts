package ttsvetkov.tradingfluts;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FlutTradeServiceTest {
    private static FlutTradeService tradeService;
    private static Schuur schuur1;
    private static Schuur schuur2;


    @BeforeClass
    public static void initFlutTradeService() {
        tradeService = new FlutTradeServiceImpl();

        // Init schuur1
        schuur1 = new Schuur(1);

        // 12 3 10 7 16 5
        Pile p = new Pile();
        p.add(new Flut(12));
        p.add(new Flut(3));
        p.add(new Flut(10));
        p.add(new Flut(7));
        p.add(new Flut(16));
        p.add(new Flut(5));

        schuur1.addPile(p);

        // Init schuur2
        schuur2 = new Schuur(2);

        // 7 3 11 9 10
        Pile p1 = new Pile();
        p1.add(new Flut(7));
        p1.add(new Flut(3));
        p1.add(new Flut(11));
        p1.add(new Flut(9));
        p1.add(new Flut(10));

        schuur2.addPile(p1);

        // 1 2 3 4 10 16 10 4 16
        Pile p2 = new Pile();
        p2.add(new Flut(1));
        p2.add(new Flut(2));
        p2.add(new Flut(3));
        p2.add(new Flut(4));
        p2.add(new Flut(10));
        p2.add(new Flut(16));
        p2.add(new Flut(10));
        p2.add(new Flut(4));
        p2.add(new Flut(16));

        schuur2.addPile(p2);
    }

    @Test
    public void testProcessSuccess1() {
        final List<List<Integer>> expectedProfitList = new ArrayList<List<Integer>>(1);

        expectedProfitList.add(new ArrayList<Integer>(
            Arrays.asList(8, -2, 5, 5, 8, 2, 7)
        ));

        final Integer expectedMaxProfit = 8;

        tradeService.process(schuur1);

        assertEquals(expectedProfitList, tradeService.getProfitList());
        assertEquals(expectedMaxProfit, tradeService.getMaxProfit());
    }

    @Test
    public void testProcessSuccess2() {
        final List<List<Integer>> expectedProfitList = new ArrayList<List<Integer>>(1);
        expectedProfitList.add(new ArrayList<Integer>(
            Arrays.asList(10, 3, 10, 9, 10, 10)
        ));

        expectedProfitList.add(new ArrayList<Integer>(
            Arrays.asList(30, 9, 17, 24, 30, 30, 24, 24, 30, 24)
        ));

        final Integer expectedMaxProfit = 40;

        tradeService.process(schuur2);

        assertEquals(expectedProfitList, tradeService.getProfitList());
        assertEquals(expectedMaxProfit, tradeService.getMaxProfit());
    }

    @Test
    public void testGetBuyListSuccess1() {
        List<Integer> expected = new ArrayList<Integer>(
            Arrays.asList(4)
        );

        tradeService.process(schuur1);

        assertEquals(expected, tradeService.getBuyList());
    }

    @Test
    public void testGetBuyListSuccess2() {
        List<Integer> expected = new ArrayList<Integer>(
            Arrays.asList(6, 7, 8, 9, 10, 12, 13)
        );

        tradeService.process(schuur2);

        assertEquals(expected, tradeService.getBuyList());
    }
}