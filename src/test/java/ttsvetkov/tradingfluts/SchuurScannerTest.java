package ttsvetkov.tradingfluts;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;

public class SchuurScannerTest { 
    private static Pile expectedPile;

    @BeforeClass
    public static void initSchuurScannerTest() {
        expectedPile = new Pile();
        expectedPile.add(new Flut(12));
        expectedPile.add(new Flut(3));
        expectedPile.add(new Flut(10));
        expectedPile.add(new Flut(7));
        expectedPile.add(new Flut(16));
        expectedPile.add(new Flut(5));
    }

    @Test
    public void testIntSuccess() {
        SchuurScanner sc = new SchuurScannerImpl("  25");
        assertTrue( 25 == sc.nextInt() );
    }

    @Test(expected = NoSuchElementException.class)
    public void testIntNoSuchElementException() {
        SchuurScanner sc = new SchuurScannerImpl("");
        sc.nextInt();
    }

    @Test
    public void testPileSuccess() {
        SchuurScanner sc = new SchuurScannerImpl("6 12 3 10 7 16 5");
        Pile pile = sc.nextPile();

        assertEquals(expectedPile, pile);
    }

    @Test
    public void testPileSuccessTrim() {
        SchuurScanner sc = new SchuurScannerImpl(" 6 \t12   3   10\t7   16 5 ");
        Pile pile = sc.nextPile();

        assertEquals(expectedPile, pile);
    }

    @Test(expected = RuntimeException.class)
    public void testPileExceptionZero() {
        SchuurScanner sc = new SchuurScannerImpl("0 12 3 10 7 16 5");
        sc.nextPile();
    }

    @Test(expected = RuntimeException.class)
    public void testPileExceptionOver() {
        SchuurScanner sc = new SchuurScannerImpl("7 12 3 10 7 16 5");
        sc.nextPile();
    }

    @Test
    public void testSchuurSuccess1Pile() {
        SchuurScanner sc = new SchuurScannerImpl("6 12 3 10 7 16 5");
        Schuur schuur = sc.nextSchuur(1);

        assertEquals(1, schuur.countPiles());
    }

    @Test
    public void testSchuurSuccess3Piles() {
        SchuurScanner sc = new SchuurScannerImpl(
            "6 12 3 10 7 16 5\n"
            + "6 12 3 10 7 16 5\n"
            + "6 12 3 10 7 16 5\n"
        );
        Schuur schuur = sc.nextSchuur(3);

        assertEquals(3, schuur.countPiles());
    }
}